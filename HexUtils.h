#ifndef ESP8266_UTILS_H
#define ESP8266_UTILS_H

#include <Arduino.h>

typedef uint32_t timestamp;


class HexUtils {

public:
    static String millisecondsToString(timestamp milliseconds);

    static char getHexChar(uint8_t hexValue);

    static void putByteHexViewToCharsArray(uint8_t value, char* charsArray, uint32_t offset);

    static void putBytesHexViewToCharsArray(uint8_t* bytes, uint32_t bytesCount, char* charsArray);


};


#endif //ESP8266_UTILS_H
