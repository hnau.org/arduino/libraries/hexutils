#include "HexUtils.h"

char HexUtils::getHexChar(uint8_t hexValue) {
    if (hexValue < 0xA) {
        return '0' + hexValue;
    }
    return 'A' + (hexValue - 0xA);
}

void HexUtils::putByteHexViewToCharsArray(
        uint8_t value,
        char* charsArray,
        uint32_t offset
) {
    charsArray[offset] = getHexChar((value >> 4) & 0xf);
    charsArray[offset + 1] = getHexChar(value & 0xf);
}

void HexUtils::putBytesHexViewToCharsArray(
        uint8_t* bytes,
        uint32_t bytesCount,
        char* charsArray
) {
    charsArray[bytesCount * 2] = 0;
    for (uint32_t i = 0; i < bytesCount; i++) {
        uint8_t value = bytes[i];
        putByteHexViewToCharsArray(value, charsArray, i * 2);
    }
}
